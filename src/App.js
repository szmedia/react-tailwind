import './App.css';
import TeaserSlider from './components/teaser-slider/teaserSliderCompontent';
import IntroAnimation from './components/intro-animation/introAnimationCompontent';
import TopicPanel from './components/topic-panel/topicPanelCompontent';

function App() {
  return (
    <div className="App">
      <TopicPanel />
      <TeaserSlider />
      <IntroAnimation />
    </div>
  );
}

export default App;
