import React from 'react';
import Button from '../button/buttonComponent';
import { ReactComponent as LogoSvg } from '../../assets/icons/logo.svg'

// Component options

// Compontent Class
class TopicPanel extends React.Component {
    render() {
        return (
            <div className='container mx-auto z-30 relative mb-16 bg-[#3e384c]  p-8 shadow-md transition-all shadow-primary hover:shadow-lg hover:shadow-primary rounded-2xl'>

                <div className='flex gap-8 flex-wrap xl:flex-nowrap'>

                    <div className='break-inside-avoid w-full xl:w-auto grow lg:shrink-0 mb-16 xl:mb-0'>
                        <LogoSvg className='mb-8 max-w-full' />
                        <p className='mb-8 lg:max-w-[400px]'>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse laboriosam voluptas voluptate, alias dolore enim explicabo voluptates debitis. Unde, perferendis.
                        </p>
                        <Button label='Get the code' variant='secondary' link="https://gitlab.com/szmedia/react-tailwind" target="_blank" />
                    </div>

                    <div className='break-inside-avoid w-full lg:w-[320px] grow xl:grow-0 mb-16 xl:mb-0'>
                        <h2 className='text-secondary font-bold text-xl pb-4 border-b-4 mb-6'>Fonts</h2>
                        <p className='font-bold text-3xl mb-8'>Roboto Slab</p>
                        <Button label='Get font' variant='secondary' link='https://fonts.google.com/specimen/Roboto+Slab' target="_blank" />
                    </div>

                    <div className='break-inside-avoid w-full lg:w-[320px] grow xl:grow-0'>
                        <h2 className='text-secondary font-bold text-xl pb-4 border-b-4 mb-6'>Colors</h2>

                        <div className='group'>
                            <div className='bg-white h-12 outline outline-4 outline-white/10 group-hover:outline-white/20 group-hover:outline-8 transition-all'></div>
                            <div className='bg-white font-bold transition-all relative top-0 group-hover:top-2 rounded-sm text-primary text-xs px-2 py-1 text-center max-w-max mx-auto -mt-3 uppercase mb-4 select-all'>#ffffff</div>
                        </div>
                        <div className='group'>
                            <div className='bg-primary h-12 outline outline-4 outline-white/10 group-hover:outline-white/20 group-hover:outline-8 transition-all'></div>
                            <div className='bg-white font-bold transition-all relative top-0 group-hover:top-2 rounded-sm text-primary text-xs px-2 py-1 text-center max-w-max mx-auto -mt-3 uppercase mb-4 select-all'>#282138</div>
                        </div>
                        <div className='group'>
                            <div className='bg-secondary h-12 outline outline-4 outline-white/10 group-hover:outline-white/20 group-hover:outline-8 transition-all'></div>
                            <div className='bg-white font-bold transition-all relative top-0 group-hover:top-2 rounded-sm text-primary text-xs px-2 py-1 text-center max-w-max mx-auto -mt-3 uppercase select-all'>#62eee7</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

// Compontent property interface

// Compotent export
export default TopicPanel;