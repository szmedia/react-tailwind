import React from 'react';

// Component options

// Compontent Class
class IntroAnimation extends React.Component {
    render() {
        return (
            <div className='fixed bottom-[200px] left-0 transform translate-y-1/2 -rotate-[15deg] z-20 w-[600px] h-[1200px] flex flex-col justify-center'>
                <div className='space-y-4'>
                    <div className='relative max-w-max'>
                        <div className='pl-[250px] hover:pl-[240px] transition-all px-12 h-20 flex text-center items-center text-3xl bg-secondary text-white uppercase font-bold'>Build</div>
                        <div className='absolute -z-10 top-0 left-full transform -translate-x-1/2 h-20 w-20 bg-secondary p4 rounded-full'></div>
                    </div>
                    <div className='relative max-w-max'>
                        <div className='pl-[260px] hover:pl-[220px] transition-all px-8 h-20 flex text-center items-center text-3xl bg-secondary text-white uppercase font-bold'>something</div>
                        <div className='absolute -z-10 top-0 left-full transform -translate-x-1/2 h-20 w-20 bg-secondary p4 rounded-full'></div>
                    </div>
                    <div className='relative max-w-max'>
                        <div className='pl-[250px] hover:pl-[220px] transition-all px-8 h-20 flex text-center items-center text-3xl bg-secondary text-white uppercase font-bold'>with</div>
                        <div className='absolute -z-10 top-0 left-full transform -translate-x-1/2 h-20 w-20 bg-secondary p4 rounded-full'></div>
                    </div>
                </div>
                <div className='absolute -z-10 transform top-1/2 left-0 -translate-y-1/2 -translate-x-1/2 bg-secondary w-[500px] h-[500px] rounded-full'></div>
                <div className='absolute -z-20 transform top-1/2 left-0 -translate-y-1/2 -translate-x-1/2 bg-white/10 w-[800px] h-[800px] rounded-full'></div>
                <div className='absolute -z-30 transform top-1/2 left-0 -translate-y-1/2 -translate-x-1/2 bg-[#3e384c] w-[1200px] h-[1200px] rounded-full'></div>
            </div>
        )
    }
}

// Compontent property interface

// Compotent export
export default IntroAnimation;