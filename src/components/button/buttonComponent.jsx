import React from 'react';
import PropTypes from 'prop-types';

// Component options
const options = {
    variants: ['primary', 'secondary'],
    sizes: ['sm', 'lg']
}

const classes = {
    variants: {
        primary: ' bg-primary text-white rounded-md w-full hover:text-secondary outline outline-0 hover:outline-4 active:outline-0 active:text-white',
        secondary: 'bg-transparent border-2 border-secondary text-white rounded-md active:border-white active:outline-0 active:text-white hover:bg-black/10 hover:text-secondary hover:outline-8 outline outline-0 outline-white/20'
    }
}

// Compontent Class
class Button extends React.Component {
    render() {
        return (
            <a href={this.props.link} target={this.props.target} className={`flex max-w-min py-4 cursor-pointer px-8 font-bold leading-none uppercase whitespace-nowrap transition-all ${classes.variants[this.props.variant]}`}>
                {this.props.label}
            </a>
        )
    }
}

// Compontent property interface
Button.propTypes = {
    label: PropTypes.string,
    variant: PropTypes.oneOf(options.variants),
    size: PropTypes.oneOf(options.sizes),
    target: PropTypes.string,
    link: PropTypes.string
};

// Compotent export
export default Button;