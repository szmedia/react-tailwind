import React from 'react';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext, DotGroup } from 'pure-react-carousel';
import Button from '../button/buttonComponent';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { ReactComponent as LogoReact } from '../../assets/icons/brands/react.svg';
import { ReactComponent as LogoAdobeXd } from '../../assets/icons/brands/adobe-xd.svg';
import { ReactComponent as LogoTailwind } from '../../assets/icons/brands/tailwind.svg';
import { ReactComponent as ChevronRight } from '../../assets/icons/chevron-right.svg';
import './slider.css';

// Component options

// Compontent Class
class TeaserSlider extends React.Component {
    render() {
        return (
            <div className='container mb-[500px] xl:mb-0 z-30 xl:z-10 mx-auto relative flex justify-end p-8 bg-black/20 shadow-md transition-all shadow-primary hover:shadow-lg hover:shadow-primary rounded-2xl'>
                <div className='xl:w-1/2 w-full xl:mx-16'>

                    <CarouselProvider
                        className='relative'
                        isIntrinsicHeight={true}
                        totalSlides={3}
                        visibleSlides={2}
                    >
                        <Slider className='pb-4'>
                            <Slide index={0}>
                                <div className='relative group bg-white  text-primary rounded-md mx-4 transition-all shadow-[5px_5px_0px_0_#fff] shadow-secondary hover:shadow-[0px_0px_0px_0_#fff] hover:shadow-secondary'>
                                    <div className='p-1 pb-0'>
                                        <div className='bg-[#00D8FF] aspect-[16/9] w-full flex items-center justify-center'>
                                            <LogoReact className='max-w-full w-1/3 text-white group-hover:scale-110 transition-all' />
                                        </div>
                                    </div>
                                    <div className='p-8'>
                                        <div className='text-secondary uppercase font-bold'>Javascript Framework</div>
                                        <div className='font-bold text-2xl mb-8'>React - A JavaScript library for building user interfaces</div>
                                        <Button variant="primary" label="Visit website" link="https://reactjs.org/" target="_blank"></Button>
                                    </div>
                                </div>
                            </Slide>
                            <Slide index={1}>
                                <div className='relative group bg-white text-primary rounded-md mx-4 transition-all shadow-[5px_5px_0px_0_#fff] shadow-secondary hover:shadow-[0px_0px_0px_0_#fff] hover:shadow-secondary'>
                                    <div className='p-1 pb-0'>
                                        <div className='bg-gradient-to-b from-[#2383AE] to-[#6DD7B9] aspect-[16/9] w-full flex items-center justify-center'>
                                            <LogoTailwind className='max-w-full w-1/3 text-white group-hover:scale-110 transition-all' />
                                        </div>
                                    </div>
                                    <div className='p-8'>
                                        <div className='text-secondary uppercase font-bold'>CSS Framework</div>
                                        <div className='font-bold text-2xl mb-8'>TailwindCSS - A utility-first framework that can  build anything in html</div>
                                        <Button variant="primary" label="Visit website" link="https://tailwindcss.com/" target="_blank"></Button>
                                    </div>
                                </div>
                            </Slide>
                            <Slide index={2}>
                                <div className='relative group bg-white text-primary rounded-md mx-4 transition-all shadow-[5px_5px_0px_0_#fff] shadow-secondary hover:shadow-[0px_0px_0px_0_#fff] hover:shadow-secondary'>
                                    <div className='p-1 pb-0'>
                                        <div className='bg-[#470137] aspect-[16/9] w-full flex items-center justify-center'>
                                            <LogoAdobeXd className='max-w-full w-1/3 text-white group-hover:scale-110 transition-all' />
                                        </div>
                                    </div>
                                    <div className='p-8'>
                                        <div className='text-secondary uppercase font-bold'>UX Design-Tool</div>
                                        <div className='font-bold text-2xl mb-8'>Adobe XD is a one-stop app, from wireframes all the way to handing off to developers. </div>
                                        <Button variant="primary" label="Visit website" link="https://www.adobe.com/de/products/xd.html" target="_blank"></Button>
                                    </div>
                                </div>
                            </Slide>
                        </Slider>
                        <ButtonBack className='absolute invisible xl:visible -left-4 -translate-x-full top-1/2 -translate-y-1/2 bg-white rounded-full text-primary h-[40px] w-[40px] flex items-center justify-center transition-all hover:scale-125 active:scale-100 hover:outline-8 outline outline-0 active:bg-secondary outline-white/10 active:outline-0'>
                            <ChevronRight className='rotate-180 h-[24px] w-auto -translate-x-[2px]' />
                        </ButtonBack>
                        <ButtonNext className='absolute invisible xl:visible -right-4 translate-x-full top-1/2 -translate-y-1/2 bg-white rounded-full text-primary h-[40px] w-[40px] flex items-center justify-center transition-all hover:scale-125 active:scale-100 hover:outline-8 outline outline-0 active:bg-secondary outline-white/10 active:outline-0'>
                            <ChevronRight className='h-[24px] w-auto translate-x-[1px]' />
                        </ButtonNext>
                        <DotGroup />
                    </CarouselProvider>
                </div>
            </div>
        )
    }
}

// Compontent property interface

// Compotent export
export default TeaserSlider;