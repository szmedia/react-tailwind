const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    "./{src,public}/**/*.{html,js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        primary: '#282138',
        secondary: '#62eee7'
      },
    },
    fontFamily: {
      'sans': ['Roboto Slab', 'ui-sans-serif', 'system-ui'],
    }
  },
  plugins: [],
}
